FROM jacksoncage/phppgadmin

MAINTAINER Alessandro Umek <alessandro.umek@blinking.eu>

RUN rm -R /var/www

RUN ln -s /usr/share/phppgadmin /var/www
 
EXPOSE 80
